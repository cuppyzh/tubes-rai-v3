-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Des 2015 pada 10.56
-- Versi Server: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tubes_rai`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE `groups` (
  `groupsid` int(11) NOT NULL,
  `groupsname` varchar(100) NOT NULL,
  `groupsdesc` text NOT NULL,
  `groupslead` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`groupsid`, `groupsname`, `groupsdesc`, `groupslead`) VALUES
(28, 'sdfsdf', 'sdfsdf', 'cuppyzh'),
(29, 'sdfsdf', 'sdfsdf', 'cuppyzh'),
(30, 'ASDASD', 'ASDAsdasd', 'cuppyzh'),
(31, 'ASDASD', 'ASDAsdasd', 'cuppyzh'),
(32, 'ASDASD', 'ASDAsdasd', 'cuppyzh'),
(33, 'ASDASD', 'ASDAsdasd', 'cuppyzh'),
(34, 'asd', 'asd', 'cuppyzh'),
(35, 'sdf', 'asd', 'cuppyzh'),
(36, 'ASd', 'asd', 'cuppyzh'),
(37, 'XX', 'XX', 'cuppyzh'),
(38, 'XXXXX', 'XXXXXX', 'cuppyzh'),
(39, 'a123', 'a123', 'cuppyzh'),
(40, '11', '11', 'cuppyzh'),
(41, '1', '1', 'cuppyzh'),
(43, 'ASdasd', 'asdasd', 'gege'),
(44, 'ASdasd', 'asdasd', 'gege'),
(47, 'LuphGege', 'LuphGege', 'gege'),
(48, 'gg', 'gg', 'gege');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups-member`
--

CREATE TABLE `groups-member` (
  `groupsid` int(11) NOT NULL,
  `userid` varchar(100) NOT NULL,
  `idgm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `groups-member`
--

INSERT INTO `groups-member` (`groupsid`, `userid`, `idgm`) VALUES
(43, 'gege', 39),
(44, 'gege', 40),
(28, 'gege', 43),
(47, 'gege', 45),
(47, 'cuppyzh', 48),
(48, 'gege', 49);

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedule`
--

CREATE TABLE `schedule` (
  `schid` int(11) NOT NULL,
  `descr` text NOT NULL,
  `priority` varchar(50) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `userid` varchar(100) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `schedule`
--

INSERT INTO `schedule` (`schid`, `descr`, `priority`, `start`, `end`, `userid`, `groupid`) VALUES
(2, 'Hari Penting', '5', '2015-11-15', '2015-11-15', 'cuppyzh', NULL),
(3, 'Rapat HMIF', '2', '2015-12-16', '2015-12-16', 'cuppyzh', NULL),
(4, 'Ultah', '1', '2015-12-01', '2015-12-01', 'gege', NULL),
(5, 'Jaga Uas', '2', '2015-12-14', '2015-12-26', 'cuppyzh', NULL),
(6, 'Makan Bareng', '2', '2015-12-23', '2015-12-23', 'cuppyzh', 47),
(7, 'Rapat Akbar', '3', '2015-12-30', '2015-12-30', NULL, 48),
(8, '123123asdasd', '4', '2015-12-14', '2015-12-16', NULL, 47),
(9, 'Makan sendiri', '5', '2015-12-23', '2015-12-24', 'cuplz', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `userid` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`userid`, `password`, `name`, `email`, `status`) VALUES
('aaa', 'aa', 'aa', 'aa', 'active'),
('asdasdasd13', 'asdasd', 'asdasd', 'asdasd', 'active'),
('cuplz', 'asdasd', 'aditama', 'iammephilesinthedark@gmail.com', 'active'),
('cuppyzh', 'asdasd', 'Yusuf Anugrah Putra Aditama', 'yapaditama@gmail.com', 'active'),
('gege', 'gege', 'gege', 'gege', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`groupsid`);

--
-- Indexes for table `groups-member`
--
ALTER TABLE `groups-member`
  ADD PRIMARY KEY (`idgm`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `groupsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `groups-member`
--
ALTER TABLE `groups-member`
  MODIFY `idgm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `schid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
