<?php

class GroupController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getProfile(){
		
		return DB::select('select * from user where userid like ? ', array(Session::get('userid')));
	}

	public function getGroup(){
		
		return DB::select('select * from `groups`,`groups-member` where `groups`.`groupsid` = `groups-member`.`groupsid` and `groups-member`.`userid` like ? ', array(Session::get('userid')));
	}

	public function getAllGroup(){
		
		return DB::select('select * from `groups`');
	}

	public function getData(){
		return array(	'profile' => $this->getProfile(),
						'group' => $this->getGroup(),
						'allgroup' => $this->getAllGroup()
					);
	}


	public function group(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$data = $this->getData();
			return View::make('group',$data);
		}
	}

	public function creategroup(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			return View::make('/creategroup');
		}
	}

	public function creategroupnew(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			DB::table('groups')->insert(array('groupsname' => $_POST['groupsname'],'groupsdesc' => $_POST['groupsdesc'],'groupslead' => Session::get('userid')));
			$id = DB::select('select groupsid from groups order by groupsid desc limit 1');
			DB::table('groups-member')->insert(array('groupsid' => $id[0]->groupsid, 'userid' => Session::get('userid')));
			$data = $this->getData();
			$data['msg'] = " Group ".$_POST['groupsname']." berhasil dibuat!";

			return Redirect::to('/group/viewgroup/'.$id[0]->groupsid)->with($data);
		}
	}

	public function leavegroup($id){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			DB::table('groups-member')->where('userid', 'like', array(Session::get('userid')))->where('groupsid','=',$id)->delete();
			
			$name = DB::select('select * from groups where groupsid = ?', array($id));

			$data = $this->getData();
			$data['msg'] = " Group ".$name[0]->groupsname." telah ditinggalkan";

			return Redirect::to('/group')->with($data);
		}
	}

	public function joingroup($id){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			//$data = $this->getData();
			//$data['group'] = DB::select('select * from `groups`,`user`,`groups-member` where `groups`.`groupsid` = `groups-member`.`groupsid` and `user`.`userid` like ? ', array(Session::get('userid')));
			$data['check'] = DB::select('select * from `groups-member` where `groups-member`.userid like ? and `groups-member`.groupsid = ?', array(Session::get('userid'), $id));

			if ($data['check'] == null){
				DB::table('groups-member')->insert(array('groupsid' => $id, 'userid' => Session::get('userid')));
			}

			$name = DB::select('select * from groups where groupsid = ?', array($id));

			
			$data = $this->getData();
			$data['msg'] = "Anda telah bergabung dengan grup ".$name[0]->groupsname;
			
			return Redirect::to('/group')->with($data);
		}
	}

	public function viewgroup($id){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$data = $this->getData();
			$data['targetGroup'] = DB::select('select * from groups where groupsid = ?', array($id));

			if ($data['targetGroup'][0]->groupslead == Session::get('userid')) $data['lead'] = 'lead';
			else {
				$cari = DB::select('select * from `groups-member` where `groups-member`.userid like ? and `groups-member`.groupsid = ?', array(Session::get('userid'), $id));
				if ($cari == null) $data['lead'] = 'member';
				else $data['lead'] = 'null';
			}

			$data['listMember'] = DB::select('select * from `user`,`groups-member` where `groups-member`.groupsid = ? and `groups-member`.userid like user.userid', array($id));
			
			return View::make('/groupdetail',$data);
		}
	}

	public function viewallgroup(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$data = $this->getData();
			$data['allgroup'] = DB::select('select * from `groups`');
			return View::make('/allgroup',$data);
		}
	}

	public function disbangroup($id){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$groupslead = DB::select('select * from groups where groupsid = ?', array($id));

			if ($groupslead[0]->groupslead == Session::get('userid')){

				DB::table('groups-member')->where('groupsid','=',$id)->delete();
				DB::table('groups')->where('groupsid','=',$id)->where('groupslead','like',Session::get('userid'))->delete();
				DB::table('schedule')->where('groupid','=',$id)->delete();
				
				$data = $this->getData();
				$data['msg'] = "Anda membubarkan group ".$groupslead[0]->groupsname;;
			
			return Redirect::to('/group')->with($data);
			}
		}
	}

	public function kickfromgroup($gid,$uid){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$groupslead = DB::select('select * from groups where groupsid = ?', array($gid));

			if ($groupslead[0]->groupslead == Session::get('userid')){

				DB::table('groups-member')->where('userid','=',$uid)->delete();

				$data = $this->getData();
				$data['msg'] = $uid." Telah dikeluarkan dari group ".$groupslead[0]->groupsname;;
			
			return Redirect::to('/group/viewgroup/'.$gid)->with($data);
			}
		}
	}

	public function giveleader($gid,$uid){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$groupslead = DB::select('select * from groups where groupsid = ?', array($gid));

			if ($groupslead[0]->groupslead == Session::get('userid')){

				DB::table('groups')->where('groupslead', 'like', Session::get('userid'))
									->where('groupsid', $gid)
									->update(array('groupslead' => $uid));

				$data = $this->getData();
				$data['msg'] = $uid." Telah diangkat menjadi ketua grup ".$groupslead[0]->groupsname;;
			
			return Redirect::to('/group/viewgroup/'.$gid)->with($data);
			}
		}
	}

}
