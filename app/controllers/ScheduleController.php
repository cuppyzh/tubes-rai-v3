<?php

class ScheduleController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controlleradds instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getProfile(){

		return DB::select('select * from user where userid like ? ', array(Session::get('userid')));
	}

	public function getGroup(){

		return DB::select('select * from `groups`,`groups-member` where `groups`.`groupsid` = `groups-member`.`groupsid` and `groups-member`.`userid` like ? ', array(Session::get('userid')));
	}

	public function getAllSchedule(){

		return DB::select('select * from schedule where userid like ?', array(Session::get('userid')));
	}

	public function getGroupSchedule(){

		$data = $this->getGroup();

		foreach ($data as $row) {
			# code...
		}
	}

	public function getData(){

		return array(	'profile' => $this->getProfile(),
						'group' => $this->getGroup(),
						'schedule' => $this->getAllSchedule(),
						'totalschedule' => $this->countSchedule()
					);
	}

	public function schedule(){

		if (Session::get('userid') == null){
			return Redirect::to('/login');
		} else {

			$data = $this->getData();
			return View::make('jadwal',$data);
		}
	}

	public function detailSchedule($sid){

		if (Session::get('userid') == null){
			return Redirect::to('/login');
		} else {

			$data = $this->getData();
			$data['schedule'] = DB::select('select * from schedule where schid = ?', array($sid));

			if ($data['schedule'][0]->priority==0 || $data['schedule'][0]->priority==1) $data['jenis'] = "personal";
			else if ($data['schedule'][0]->priority<=3) $data['jenis'] ="group";
			else $data['jenis'] ="nope";

			if ($data['schedule'][0]->priority==1 || $data['schedule'][0]->priority==3) $data['prio'] = "tinggi";
			else if ($data['schedule'][0]->priority==0 || $data['schedule'][0]->priority==2 ) $data['prio'] ="rendah";
			else $data['prio'] ="nope";

			if ($data['schedule'][0]->groupid == null) $data['prev'] = true;
			else {
				$query = DB::select('select * from groups where groupsid = ?', array($data['schedule'][0]->groupid));

				if ($query[0]->groupslead == Session::get('userid')) $data['prev'] = true;
				else $data['prev'] = false;
			}

			return View::make('detailjadwal',$data);
		}
	}

	public function createSchedule(){

		if (Session::get('userid') == null){
			return Redirect::to('/login');
		} else {
			$userid = NULL;
			$group = NULL;
			$prio = "4";

			if ($_POST['tglmulai'] == "Klik Kalender") return Redirect::to('/jadwal/add')->with('errMsg','Tanggal Mulai Salah!');

			if ($_POST['tglakhir'] == "Klik Kalender") $_POST['tglakhir'] = $_POST['tglmulai'];

			if (!isset($_POST['priority']) || !isset($_POST['jenis'])) {
				$prio = "4";
				$userid = Session::get('userid');
			} else if ($_POST['jenis']=='personal' && $_POST['priority']=='rendah') {
				$prio = "0";
				$userid = Session::get('userid');
			}
			else if ($_POST['jenis']=='personal' && $_POST['priority']=='tinggi') {
				$prio = "1";
				$userid = Session::get('userid');
			}
			else if ($_POST['jenis']=='group' && $_POST['priority']=='rendah') {
				$prio = "2";
				$group = $_POST['groupid'];
			}
			else if ($_POST['jenis']=='group' && $_POST['priority']=='tinggi') {
				$prio = "3";
				$group = $_POST['groupid'];
			}

			$newData = array(
								'descr' => $_POST['desc'],
								'priority' => $prio,
								'start' => $_POST['tglmulai'],
								'end' => $_POST['tglakhir'],
								'userid' => $userid,
								'groupid' => $group

			);

			DB::table('schedule')->insert($newData);

			return Redirect::to('/jadwal/add')->with('sucMsg','Jadwal Baru telah diinputkan!');
		}
	}

	public function listschedule(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {

			// $listSchedule = array();

			// 	$listGroup = DB::select('select * from `groups-member` where userid like ? ', array(Session::get('userid')));

			// 	foreach ($listGroup as $lg) {
			// 		$listScheduleGroup = DB::select('select * from schedule where groupid like ?', array($lg->groupsid));
			// 		$group = DB::select('select * from groups where groupsid like ?', array($lg->groupsid));

			// 		foreach ($listScheduleGroup as $row) {

			// 			if ($row->priority == '0') $prio = 'event';
			// 			else if ($row->priority == '1') $prio = 'event-inverse';
			// 			else if ($row->priority == '2') $prio = 'event-info';
			// 			else if ($row->priority == '3') $prio = 'event-special';
			// 			else if ($row->priority == '4') $prio = 'event-important';

			// 			$listSchedule[] = array(
			// 								'id' => $row->schid,
			// 								'title' => "GROUP : <a href='".$group[0]->groupsid."'>".$group[0]->groupsname."</a><br /> Agenda ".$row->descr,
			// 								'class' => $prio,
			// 								'url' => '',
			// 								'start' => $row->start,
			// 								'end' => $row->end
			// 							);
			// 		}
			// 	}

			// 	$listSchedulePersonal = DB::select('select * from schedule where userid like ? ', array(Session::get('userid')));

			// 	foreach ($listSchedulePersonal as $row) {

			// 			if ($row->priority == '0') $prio = 'event';
			// 			else if ($row->priority == '1') $prio = 'event-inverse';
			// 			else if ($row->priority == '2') $prio = 'event-info';
			// 			else if ($row->priority == '3') $prio = 'event-special';
			// 			else if ($row->priority == '4') $prio = 'event-important';

			// 		$listSchedule[] = array(
			// 								'id' => $row->schid,
			// 								'title' => $row->descr,
			// 								'class' => $prio,
			// 								'url' => '',
			// 								'start' => $row->start,
			// 								'end' => $row->end
			// 							);
			// 	}

			// 	$data = $this->getData();
			// 	$data['listSchedulePersonal'] = $listSchedule;

			// return View::make('listjadwal',$data);

			$listSchedule = array();

			$query = DB::select('select * from schedule where userid like ?', array(Session::get('userid')));

			foreach ($query as $key => $row) {

				// if ($row->priority == '0') $prio = 'event';
			 // 			else if ($row->priority == '1') $prio = 'event-inverse';
			 // 			else if ($row->priority == '2') $prio = 'event-info';
				// 		else if ($row->priority == '3') $prio = 'event-special';
			 // 			else if ($row->priority == '4') $prio = 'event-important';

				$listSchedule[] = array(
									'id' => $row->schid,
									'title' => $row->descr,
									'class' => $row->priority,
									'url' => '',
									'start' => $row->start,
									'end' => $row->end
								);
			}

			$query = DB::select('select * from `groups-member` where userid like ? ', array(Session::get('userid')));

			foreach ($query as $row) {
				$query2 = DB::select('select * from schedule where groupid = ?', array($row->groupsid));
				$group = DB::select('select * from groups where groupsid = ? ', array($row->groupsid));

				foreach ($query2 as $row2) {

					// if ($row2->priority == '0') $prio = 'event';
			 	// 		else if ($row2->priority == '1') $prio = 'event-inverse';
			 	// 		else if ($row2->priority == '2') $prio = 'event-info';
					// 	else if ($row2->priority == '3') $prio = 'event-special';
			 	// 		else if ($row2->priority == '4') $prio = 'event-important';

					$listSchedule[] = array(
									'id' => $row2->schid,
									'title' => 'GROUP : <a href="'.url().'/viewgroup/'.$group[0]->groupsid.'">'.$group[0]->groupsname.'</a><br />Agenda : '.$row2->descr,
									'class' => $row2->priority,
									'url' => '',
									'start' => $row2->start,
									'end' => $row2->end
								);
				}
			}

				//$data = $this->getData();
				$data['listSchedules'] = $listSchedule;

			return View::make('listjadwal',$data);

		}
	}

	public function listScheduleGroup($gid){

			$listSchedule = array();

			$listAnggotaGroup = DB::select('select * from `groups-member` where groupsid like ?', array($gid));
			
			foreach ($listAnggotaGroup as $row){
				$listGroupAnggota = DB::select('select * from `groups-member` where userid like ? ', array($row->userid));

				foreach ($listGroupAnggota as $lg) {
					$listScheduleGroup = DB::select('select * from schedule where groupid like ?', array($lg->groupsid));
					$group = DB::select('select * from groups where groupsid like ?', array($lg->groupsid));

					foreach ($listScheduleGroup as $rows) {

						if ($rows->priority == '0') $prio = 'event';
						else if ($rows->priority == '1') $prio = 'event-inverse';
						else if ($rows->priority == '2') $prio = 'event-info';
						else if ($rows->priority == '3') $prio = 'event-special';
						else if ($rows->priority == '4') $prio = 'event-important';

						$listSchedule[] = array(
											'id' => $rows->schid,
											'title' => "GROUP : <a href='".$group[0]->groupsid."'>".$group[0]->groupsname."</a><br /> Agenda ".$rows->descr,
											'class' => $rows->priority,
											'url' => '',
											'start' => $rows->start,
											'end' => $rows->end
										);
					}

					$jadwalPersonal = DB::select(' select * from schedule where userid like ? ', array($row->userid));

					foreach ($jadwalPersonal as $lp) {

						if ($lp->priority == '0') $prio = 'event';
						else if ($lp->priority == '1') $prio = 'event-inverse';
						else if ($lp->priority == '2') $prio = 'event-info';
						else if ($lp->priority == '3') $prio = 'event-special';
						else if ($lp->priority == '4') $prio = 'event-important';

						$user = DB::select('select * from user where userid like ?', array($row->userid));

						$listSchedule[] = array(
											'id' => $lp->schid,
											'title' => "User : ".$user[0]->userid."<br />Agenda : ".$lp->descr,
											'class' => $lp->priority,
											'url' => '',
											'start' => $lp->start,
											'end' => $lp->end
										);

					}

				}

			}

			$makeDistinctSchedule = array();
			$disctSchedule = array();

			foreach ($listSchedule as $row) {

				$flag = true;

				foreach ($makeDistinctSchedule as $dir) {
					if ($row['id'] == $dir['id']) $flag = false;
				}

				if ($flag == true) {
					$disctSchedule[] = $row;
					$makeDistinctSchedule[] = $row;
				}
			}

			// echo json_encode(array('gid' => $gid, 'success' => 1, 'result' => $disctSchedule));
			$q = DB::select('select * from groups where groupsid = ?', array($gid));
			$data['groupname'] = $q[0]->groupsname;
			 $data['schedule'] = $disctSchedule;
			return View::make('listjadwalgroup',$data);

		
	}

	public function scheduleByGroupMember(){

		if (Session::get('userid') == null){

			return Redirect::to('/login');
		} else {
			Session::set('gid',$_POST['groupid']);
			$data = $this->getData();
			$data['group'] = DB::select('select * from groups where groupsid = ?', array($_POST['groupid']));

			return View::make('jadwalgroup',$data);
		}
	}

	public function scheduleByGroupMember2($gid){

		if (Session::get('userid') == null){

			return Redirect::to('/login');
		} else {
			$data = $this->getData();
			$data['group'] = DB::select('select * from groups where groupsid = ?', array($gid));
			Session::set('gid',$gid);
			return View::make('jadwalgroup',$data);
		}
	}

	public function scheduleAdd(){

		if (Session::get('userid') == null){

			return Redirect::to('/login');
		} else {
			$data = $this->getData();

			return View::make('addjadwal',$data);
		}
	}

	public function scheduleJson(){

		if (Session::get('gid') == null){
				
				$listSchedule = array();

				$listGroup = DB::select('select * from `groups-member` where userid like ? ', array(Session::get('userid')));

				foreach ($listGroup as $lg) {
					$listScheduleGroup = DB::select('select * from schedule where groupid like ?', array($lg->groupsid));
					$group = DB::select('select * from groups where groupsid like ?', array($lg->groupsid));

					foreach ($listScheduleGroup as $row) {

						if ($row->priority == '0') $prio = 'event';
						else if ($row->priority == '1') $prio = 'event-inverse';
						else if ($row->priority == '2') $prio = 'event-info';
						else if ($row->priority == '3') $prio = 'event-special';
						else if ($row->priority == '4') $prio = 'event-important';

						$listSchedule[] = array(
											'id' => $row->schid,
											'title' => "GROUP : ".$group[0]->groupsname." - ".$row->descr,
											'class' => $prio,
											'url' => '',
											'start' => strtotime($row->start) .'000',
											'end' => strtotime($row->end) .'000'
										);
					}
				}

				$listSchedulePersonal = DB::select('select * from schedule where userid like ? ', array(Session::get('userid')));

				foreach ($listSchedulePersonal as $row) {

						if ($row->priority == '0') $prio = 'event';
						else if ($row->priority == '1') $prio = 'event-inverse';
						else if ($row->priority == '2') $prio = 'event-info';
						else if ($row->priority == '3') $prio = 'event-special';
						else if ($row->priority == '4') $prio = 'event-important';

					$listSchedule[] = array(
											'id' => $row->schid,
											'title' => $row->descr,
											'class' => $prio,
											'url' => '',
											'start' => strtotime($row->start) .'000',
											'end' => strtotime($row->end) .'000'
										);
				}

				echo json_encode(array('success' => 1, 'result' => $listSchedule));
		} else {
			
						$listSchedule = array();

			$gid = Session::get('gid');

			$listAnggotaGroup = DB::select('select * from `groups-member` where groupsid like ?', array($gid));
			
			foreach ($listAnggotaGroup as $row){
				$listGroupAnggota = DB::select('select * from `groups-member` where userid like ? ', array($row->userid));

				foreach ($listGroupAnggota as $lg) {
					$listScheduleGroup = DB::select('select * from schedule where groupid like ?', array($lg->groupsid));
					$group = DB::select('select * from groups where groupsid like ?', array($lg->groupsid));

					foreach ($listScheduleGroup as $rows) {

						if ($rows->priority == '0') $prio = 'event';
						else if ($rows->priority == '1') $prio = 'event-inverse';
						else if ($rows->priority == '2') $prio = 'event-info';
						else if ($rows->priority == '3') $prio = 'event-special';
						else if ($rows->priority == '4') $prio = 'event-important';

						$listSchedule[] = array(
											'id' => $rows->schid,
											'title' => "GROUP : ".$group[0]->groupsname." | ".$rows->descr,
											'class' => $prio,
											'url' => '',
											'start' => strtotime($rows->start) .'000',
											'end' => strtotime($rows->end) .'000'
										);
					}

					$jadwalPersonal = DB::select(' select * from schedule where userid like ? ', array($row->userid));

					foreach ($jadwalPersonal as $lp) {

						if ($lp->priority == '0') $prio = 'event';
						else if ($lp->priority == '1') $prio = 'event-inverse';
						else if ($lp->priority == '2') $prio = 'event-info';
						else if ($lp->priority == '3') $prio = 'event-special';
						else if ($lp->priority == '4') $prio = 'event-important';

						$user = DB::select('select * from user where userid like ?', array($row->userid));

						$listSchedule[] = array(
											'id' => $lp->schid,
											'title' => "User : ".$user[0]->userid." | Agenda : ".$lp->descr,
											'class' => $prio,
											'url' => '',
											'start' => strtotime($lp->start) .'000',
											'end' => strtotime($lp->end) .'000' 
										);

					}

				}

			}

			$makeDistinctSchedule = array();
			$disctSchedule = array();
			$disctSchedule[] = array(
											'id' => '0',
											'title' => "User : ".$user[0]->userid." | Agenda : ".$lp->descr,
											'class' => $prio,
											'url' => '',
											'start' => $lp->start,
											'end' => $lp->end
										);

			foreach ($listSchedule as $row) {

				$flag = true;

				foreach ($makeDistinctSchedule as $dir) {
					if ($row['id'] == $dir['id']) $flag = false;
				}

				if ($flag == true) {
					$disctSchedule[] = $row;
					$makeDistinctSchedule[] = $row;
				}
			}

			Session::forget('gid');

			echo json_encode(array('success' => 1, 'result' => $disctSchedule));

			//Session::forget('gid');

		}
	}

	public function countSchedule(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {

			

			$listSchedule = array();

			$query = DB::select('select * from schedule where userid like ?', array(Session::get('userid')));

			foreach ($query as $key => $row) {

			

				$listSchedule[] = array(
									'id' => $row->schid,
									'title' => $row->descr,
									'class' => $row->priority,
									'url' => '',
									'start' => $row->start,
									'end' => $row->end
								);
			}

			$query = DB::select('select * from `groups-member` where userid like ? ', array(Session::get('userid')));

			foreach ($query as $row) {
				$query2 = DB::select('select * from schedule where groupid = ?', array($row->groupsid));
				$group = DB::select('select * from groups where groupsid = ? ', array($row->groupsid));

				foreach ($query2 as $row2) {

					$listSchedule[] = array(
									'id' => $row2->schid,
									'title' => 'GROUP : <a href="'.url().'/viewgroup/'.$group[0]->groupsid.'">'.$group[0]->groupsname.'</a><br />Agenda : '.$row2->descr,
									'class' => $row2->priority,
									'url' => '',
									'start' => $row2->start,
									'end' => $row2->end
								);
				}
			}

				//$data = $this->getData();
				$data['listSchedules'] = $listSchedule;

			return sizeof($listSchedule);
		}
	}

}
