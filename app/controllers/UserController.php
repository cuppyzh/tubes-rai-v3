<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function sessionOn() { //gak bisa dipake, cek manual

		if (Session::get('userid') == null) {
			return false;
		} else return true;
	}

	public function register(){

		if (Session::get('userid') != null) {
			//echo Session::get('userid');
			return Redirect::to('/');
		}
		else {
			return View::make('register');
		}
	}

	public function valRegister(){

		$results = DB::select('select * from user where userid like ?', array($_POST['username']));

		if ($results == null) {

			if ($_POST['password'] != $_POST['password_confirm']){
				return Redirect::to('register')->with('errMessage', 'Konfirmasi password salah');
			}  else {
					DB::table('user')->insert(
					    array(	'userid' => $_POST['username'], 
					    		'password' => $_POST['password'],
					    		'name' => $_POST['fullname'],
					    		'email' => $_POST['email'],
					    		'status' => 'active')
					);
					
					return Redirect::to('login')->with('sucMessage', 'Pembuatan akun sukses, silahkan login');
			}
		} else {
			return Redirect::to('register')->with('errMessage', 'Username sudah digunakan');
		}

	}

	public function login(){

		if (Session::get('userid') != null) {
			return Redirect::to('/');
		}
		else {
			return View::make('login');
		}
	}

	public function valLogin(){
		
		$results = DB::select('select * from user where userid like ? and password like ?', array($_POST['username'], $_POST['password']));

		if ($results == null){
			
			$data['errMessage'] = 'Username dan Password tidak ditemukan';
			return Redirect::to('login')->with('errMessage', 'Username dan Password tidak ditemukan');
		} else {
			Session::put('userid', $_POST['username']);
			return Redirect::to('/');
		}
	}

	public function logout(){
		
		Session::forget('userid');
		return Redirect::to('/');
	}

	public function profile(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {
			$data['profile'] = DB::select('select * from user where userid like ? ', array(Session::get('userid')));
			$data['group'] = DB::select('select * from `groups`,`user`,`groups-member` where `groups`.`groupsid` = `groups-member`.`groupsid` and `user`.`userid` like ? ', array(Session::get('userid')));
			$query = DB::select('select * from `groups-member` where userid like ? ', array(Session::get('userid')));
			$data['numofgroups'] = sizeof($query);
			$data['numofschedule'] = sizeof(DB::select('select * from schedule where userid like ? ', array(Session::get('userid'))));
			$data['totofschedule'] = $this->counts();

			return View::make('profile',$data);
		}
	}

	public function counts(){

		if (Session::get('userid') == null) return Redirect::to('/login');
		else {

			

			$listSchedule = array();

			$query = DB::select('select * from schedule where userid like ?', array(Session::get('userid')));

			foreach ($query as $key => $row) {

			

				$listSchedule[] = array(
									'id' => $row->schid,
									'title' => $row->descr,
									'class' => $row->priority,
									'url' => '',
									'start' => $row->start,
									'end' => $row->end
								);
			}

			$query = DB::select('select * from `groups-member` where userid like ? ', array(Session::get('userid')));

			foreach ($query as $row) {
				$query2 = DB::select('select * from schedule where groupid = ?', array($row->groupsid));
				$group = DB::select('select * from groups where groupsid = ? ', array($row->groupsid));

				foreach ($query2 as $row2) {

					$listSchedule[] = array(
									'id' => $row2->schid,
									'title' => 'GROUP : <a href="'.url().'/viewgroup/'.$group[0]->groupsid.'">'.$group[0]->groupsname.'</a><br />Agenda : '.$row2->descr,
									'class' => $row2->priority,
									'url' => '',
									'start' => $row2->start,
									'end' => $row2->end
								);
				}
			}

				//$data = $this->getData();
				$data['listSchedules'] = $listSchedule;

			return sizeof($listSchedule);
		
	}
}

}
