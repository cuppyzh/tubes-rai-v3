<!DOCTYPE html>
<html>

	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
			echo View::make('linker/linker_main_template')->render();
		?>
	</head>

	<body>
		<?php
			$data['active'] = "register";
			echo View::make('template/nav',$data)->render();
		?>
		<!-- REGISTRATION FORM -->
		<div class="container">
			<?php
				if (Session::get('errMessage')!=null) echo "<div class='errMsg'>".Session::get('errMessage')."</div><br />";
				else if (Session::get('sucMessage')) echo "<div class='sucMsg'>".Session::get('sucMessage')."</div><br />";
			?>
			<div class="distracted">
				 <div class="well">
			  <h3 class="ghj" style="width"><center>Daftar Baru</center></h3>
				  
						<form method="post" action="<?php echo url(); ?>/register/validate">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Username</span>
								<input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" name="username">
							</div>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Password</span>
								<input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" name="password">
							</div>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Konfirmasi Password</span>
								<input type="password" class="form-control" placeholder="Konfirmasi Password" aria-describedby="basic-addon1" name="password_confirm">
							</div>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Email</span>
								<input type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon1" name="email">
							</div>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Nama Lengkap</span>
								<input type="text" class="form-control" placeholder="Nama Lengkap" aria-describedby="basic-addon1" name="fullname">
							</div>

							<h3 class="b3">
								<button class="label label-success" type="submit">Daftar</button>
				  			</h2>


				   </div>
				  
		    </div>
			<!-- 
			<div class="login-form-1">
				<form id="register-form" class="text-left" method="post" action="<?php echo url(); ?>/register/validate">
					<div class="login-form-main-message"></div>
					<div class="main-login-form">
						<div class="login-group">
							<div class="form-group">
								<label for="reg_username" class="sr-only">Email address</label>
								<input type="text" class="form-control" id="reg_username" name="username" placeholder="Username">
							</div>
							<div class="form-group">
								<label for="reg_password" class="sr-only">Password</label>
								<input type="password" class="form-control" id="reg_password" name="password" placeholder="Password">
							</div>
							<div class="form-group">
								<label for="reg_password_confirm" class="sr-only">Password Confirm</label>
								<input type="password" class="form-control" id="reg_password_confirm" name="password_confirm" placeholder="Konfirmasi Password">
							</div>
							
							<div class="form-group">
								<label for="reg_email" class="sr-only">Email</label>
								<input type="text" class="form-control" id="reg_email" name="email" placeholder="Email">
							</div>
							<div class="form-group">
								<label for="reg_fullname" class="sr-only">Nama Lengkap</label>
								<input type="text" class="form-control" id="reg_fullname" name="fullname" placeholder="Nama Lengkap">
							</div>
						</div>
						<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i>O</button>
					</div>
					<div class="etc-login-form">
						<center><p>sudah punya akun? <a href="<?php echo url(); ?>/login">login disini</a></p></center>
					</div>
				</form>
			</div>
			 -->
		</div>
		<?php echo "<div class='navbar-fixed-bottom'>";
			echo View::make('template/foo',$data)->render();
		"</div>"?>
	</body>

</html>