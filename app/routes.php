<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('main');
});

Route::get('/aboutus', function(){
	return View::make('aboutus');
});


Route::get('/register', 'UserController@register');
Route::post('/register/validate', 'UserController@valRegister');


Route::get('/login', 'UserController@login');
Route::post('/login/validate', 'UserController@valLogin');

Route::get('/logout', 'UserController@logout');

Route::get('/profile', 'UserController@profile');

Route::get('/group', array('as' => 'group', 'uses' => 'GroupController@group'));
Route::get('/group/creategroup', 'GroupController@creategroup');
Route::post('/group/creategroup/new', 'GroupController@creategroupnew');
Route::get('/group/viewall', 'GroupController@viewallgroup');
Route::get('/group/viewgroup/{id}', array('as' => 'viewgroup', 'uses'=> 'GroupController@viewgroup'));
Route::get('/group/leavegroup/{id}', 'GroupController@leavegroup');
Route::get('/group/joingroup/{id}', 'GroupController@joingroup');
Route::get('/group/disbangroup/{id}', 'GroupController@disbangroup');
Route::get('/group/kickfromgroup/{gid}/{uid}', 'GroupController@kickfromgr	oup');
Route::get('/group/giveleaderto/{gid}/{uid}', 'GroupController@giveleader');

Route::get('/jadwal', 'ScheduleController@schedule');
Route::get('/jadwal/detail/{sid}', '');
Route::get('/jadwal/list', 'ScheduleController@listschedule');
Route::post('/jadwal/group', function(){
	if (!isset($_POST['groupid'])) return Redirect::to('/jadwal');
	return Redirect::to('/jadwal/group/'.$_POST['groupid']);
});
Route::get('/jadwal/group/{gid}', 'ScheduleController@scheduleByGroupMember2');
Route::get('/jadwal/add', 'ScheduleController@scheduleAdd');
Route::get('/jadwal/group/list/{gid}', 'ScheduleController@listscheduleGroup');
Route::post('/jadwal/add/make', 'ScheduleController@createSchedule');
Route::get('/jadwal/detail/{sid}', 'ScheduleController@detailSchedule');

Route::get('/shortcode', function(){
	return View::make('shortcode');
});

//Testing route for something..............
Route::get('/jadwaljson', 'ScheduleController@scheduleJson');

Route::get('/testing', function(){
	return View::make('testing');
});