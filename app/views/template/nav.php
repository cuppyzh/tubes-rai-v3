<div class="header navbar-fixed-top">
	 <div class="container">
		 <div class="header-top">
		 		<img class="img-logo pull-left" style="height:60px; width:75px; padding:5px;" src="<?php echo url(); ?>/assets2/images/sinkingship.png">
				<div class="logo">
					<?php echo "<a href='".url()."/aboutus'><h1>Sinking Ship.dev <span>Pencari Jadwal Kosong Untuk Janjian</span></h1></a> "?>
				</div>
				<div class="top-menu">
					<span class="menu">MENU</span>
					<ul>
						<li <?php if ($active == 'home') echo "class='active'"; ?>><a href="<?php echo url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Beranda</a></li>
						<li <?php if ($active == 'profile') echo "class='active'"; ?>><a href="<?php echo url(); ?>/profile"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Profil</a></li>
						<li <?php if ($active == 'group') echo "class='active'"; ?>><a href="<?php echo url(); ?>/group"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Grup</a></li>
						<li <?php if ($active == 'jadwal') echo "class='active'"; ?>><a href="<?php echo url(); ?>/jadwal"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>Jadwal</a></li>
						<li <?php if ($active == 'login') echo "class='active'"; ?>>
							<?php
								if (Session::get('userid') == null) echo "<a href='".url()."/login'><span class='glyphicon glyphicon-log-in' aria-hidden='true'></span>Login</a>";
							 	else echo "<a href='".url()."/logout'><span class='glyphicon glyphicon-log-out' aria-hidden='true'></span>Logout</a>"
							?> 	
						</li>
						<li <?php if ($active == 'register') echo "class='active'"; ?>>
								<?php
								 	if (Session::get('userid') == null) echo "<a href='".url()."/register'><span class='glyphicon glyphicon-registration-mark' aria-hidden='true'></span>Register</a>";
								?>						 	
						</li>
					 </ul>
		 		</div>
		 </div>
		 <!-- script-for-menu -->
		 <script>					
					$("span.menu").click(function(){
						$(".top-menu ul").slideToggle("slow" , function(){
						});
					});
		 </script>
		 <!-- script-for-menu -->	
	 </div>
</div>