<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
				echo View::make('linker/linker_main_template')->render();
		?>
	</head>
	
	<body>
	<!---->
		<?php
			$data['active'] = 'group';
			echo View::make('template/nav',$data)->render();
		?>
		<!---->
		<div class="about">
			 <div class="container">
				 <h2>Grup</h2>
				 

					<div class="bs-docs-example">
						 <div class="col-md-6 about-info">
						 <h3>Daftar Grup</h3>
					 </div>
						<table class="table table-hover">
							<thead>
								<tr>
								  <th width='50'>#</th>
								  <th width='100'>ID Grup</th>
								  <th width='186'>Nama Goup</th>
								  <th width='186'>Deskripsi Goup</th>
								  <th width='186'>Keterangan</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$ct=1;
									foreach($allgroup as $datas){
										//$flag=true;
											
										echo "<tr>";
										echo "<td>".$ct."</td>";
										echo "<td>".$datas->groupsid."</td>";
										echo "<td><a href='".url()."/group/viewgroup/".$datas->groupsid."'>".$datas->groupsname."</a></td>";
										echo "<td>".$datas->groupsdesc."</td>";
										
										$flag = 'Belum bergabung';

										foreach ($group as $data) {
											if ($data->groupsid == $datas->groupsid) {
												$flag = 'Sudah Bergabung';
												break;
											}
										}

										echo "<td>".$flag."</td>";
										echo "</tr>";	

										$ct++;	
										}
								?>
								
							</tbody>
						</table>
					</div>

				
				 
			 </div>
		</div>
		<!---->
		<?php
			echo View::make('template/foo')->render();
		?>
	<!---->
	</body>
</html>