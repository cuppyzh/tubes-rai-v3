<!DOCTYPE html>
<html>
<head>
    <title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>

    <?php
        echo View::make('linker/linker_main_template')->render();
    ?>

</head>
<body>

    <?php
        $data['active'] = 'jadwal';
        echo View::make('template/nav',$data)->render();
    ?>
    <div class="about">
        <div class="container" style="height:92.5%;">
            <h2>Jadwal</h2>
            <div class="alerts">
                <?php

                    if (Session::get('sucmsg')!=null) {
                        echo "
                            <div class='alert alert-success' role='alert'>
                                <strong>Done! </strong>".Session::get('msg')."
                            </div>";
                        }

                       ?>
            </div>
            <h3 class="b3">
                    <a href="<?php echo url(); ?>/jadwal/list"<span class="label label-success">List Jadwal</span></a>
                    <a href="<?php echo url(); ?>/jadwal/add"<span class="label label-success">Tambah Jadwal</span></a>
            </h3>

            <center>
                <iframe src="http://localhost:8080/tubes-rai-v3-calendar/" width="1000px" height="650px" frameborder="0"></iframe>
            </center>

            <div class="row">
                    <div class="col-lg-8 in-gp-tb">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Grup : </span>
                            <form method="post" action="<?php echo url(); ?>/jadwal/group">
                            <select name='groupid' class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown" aria-haspopup="true" aria-describedby="basic-addon1">
                                <?php
                                    foreach ($group as $row) {
                                        echo "<option value=".$row->groupsid.">".$row->groupsname."</option>";
                                    }
                                ?> 
                            </select>
                        </div>
                            <h3 class="b3"><br />
                                <span class="label label-success"><button type="submit">Jadwal Berdasarkan Group</button></span>
                            </h3>
                            </form>
                    </div>
                </div>
        </div>
    </div>
    <?php
        echo View::make('template/foo')->render();
    ?>

</body>
</html>
