
<!DOCTYPE html>
<html>
<head>
    <title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>

    <?php
        echo View::make('linker/linker_main_template')->render();
        $data['group'] = $group;
        echo View::make('linker/linker_date_picker', $data)->render();
    ?>

</head>
<body>

    <?php
        $data['active'] = 'jadwal';
        echo View::make('template/nav',$data)->render();
    ?>

    <div class="about">
        <div class="container">
            <h2>Jadwal Detail : <?php echo $schedule[0]->schid; ?></h2>
            <div class="alerts">
                <?php

                    if (Session::get('sucmsg')!=null) {
                        echo "
                            <div class='alert alert-success' role='alert'>
                                <strong>Done! </strong>".Session::get('msg')."
                            </div>";
                        }

                       ?>
            </div>

            <div class="about-grids">
                        <form method="post" action="<?php echo url();?>/jadwal/add/make">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Deskripsi Jadwal</span>
                                <input type="text" class="form-control" placeholder="Deskripsi" aria-describedby="basic-addon1" name="desc" value="<?php echo $schedule[0]->descr; ?>" <?php if ($prev == false) echo "readonly"; ?>>
                            </div>

                            <div class="row">

                                <div class="col-lg-6 in-gp-tl">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" aria-label="..." name="jenis" value="personal" <?php if ($jenis=="personal") echo "checked='checked'"; ?> <?php if ($prev == false) echo "disabled"; ?>/>
                                        </span>
                                        <input type="text" class="form-control" aria-label="..." placeholder="Personal" readonly>
                                    </div>
                                </div>

                                <div class="col-lg-6 in-gp-tb">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input class="group-check-js" type="radio" aria-label="..." name="jenis" value="group" <?php if ($jenis=="group") echo "checked='checked'"; ?> <?php if ($prev == false) echo "disabled"; ?>/>
                                        </span>
                                        <input type="text" class="form-control" aria-label="..." placeholder="Grup" readonly>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Grup : </span>
                                    <select name="groupid" class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown" aria-haspopup="true" aria-describedby="basic-addon1" <?php if ($prev == false) echo "disabled"; ?>>
                                    <?php

                                        foreach ($group as $row) {
                                            $s = "";
                                            if ($row->groupsid == $row->groupsid) $s = "selected";

                                            echo "<option value='".$row->groupsid."' ".$s.">".$row->groupsname."</option>";
                                        }
                                    ?>
                                    </select>
                            </div>

                            <!--<div id="surpriseeee">
                            </div>-->

                            <div class="row">

                                <div class="col-lg-6 in-gp-tl">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" aria-label="..." name="priority" value="rendah" <?php if ($prio=="rendah") echo "checked='checked'"; ?>  <?php if ($prev == false) echo "disabled"; ?> />
                                        </span>
                                        <input type="text" class="form-control" aria-label="..." placeholder="Rendah" readonly>
                                    </div>
                                </div>

                                <div class="col-lg-6 in-gp-tb">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" aria-label="..." name="priority" value="tinggi" <?php if ($prio=="tinggi") echo "checked='checked'"; ?>  <?php if ($prev == false) echo "disabled"; ?> />
                                        </span>
                                        <input type="text" class="form-control" aria-label="..." placeholder="Tinggi" readonly>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="input-group">
                                    <span class="<?php if ($prev == true) echo 'tglmulai'; ?>"><img src="http://localhost:8080/tubes-rai-v3/public/assets_datepicker/images/calendar.png" /></span>Tanggal Mulai
                                    <input name="tglmulai" id="tglmulaiinput" type="text" class="form-control" value="<?php if ($schedule[0]->start==NULL) echo "Klik Kalender"; else echo $schedule[0]->start ?>" readonly>

                            </div>

                            <div class="input-group">
                                    <span class="<?php if ($prev == true) echo 'tglmulai'; ?>"><img src="http://localhost:8080/tubes-rai-v3/public/assets_datepicker/images/calendar.png" /></span>Tanggal Mulai
                                    <input name="tglakhir" id="tglakhirinput" type="text" class="form-control" value="<?php if ($schedule[0]->end==NULL) echo "Klik Kalender"; else echo $schedule[0]->end ?>" aria-describedby="basic-addon1" readonly>
                            </div>
                            
                    
                            <h4 class="b4">
                                <?php
                                    if ($prev == false) {

                                    } else echo '<span class="label label-success"><button type="submit">Mutakhirkan Jadwal</button></span> ';
                                ?>
                            </h4>

                 </form>
                     
                    </div>
            
        </div>
    </div>





    <?php
        echo View::make('template/foo')->render();
    ?>

    <script src="<?php echo url(); ?>/assets_datepicker/src/datepickr.min.js"></script>
        <script>
            // datepickr on an icon, using altInput to store the value
            // altInput must be a direct reference to an input element (for now)
            datepickr('.tglmulai', { altInput: document.getElementById('tglmulaiinput') });
            datepickr('.tglakhir', { altInput: document.getElementById('tglakhirinput') });
            // If the input contains a value, datepickr will attempt to run Date.parse on it
            datepickr('[title="parseMe"]');

            // Overwrite the global datepickr prototype
            // Won't affect previously created datepickrs, but will affect any new ones
            datepickr.prototype.l10n.months.shorthand = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            datepickr.prototype.l10n.months.longhand = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            datepickr.prototype.l10n.weekdays.shorthand = ['dim', 'lun', 'mar', 'mer', 'jeu', 'ven', 'sam'];
            datepickr.prototype.l10n.weekdays.longhand = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
            datepickr('#someFrench.sil-vous-plait', { dateFormat: '\\Da\\y picke\\d: Y/m/d' });
        </script>

</body>
</html>
