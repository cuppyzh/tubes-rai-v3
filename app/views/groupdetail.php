<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
				echo View::make('linker/linker_main_template')->render();
		?>
	</head>

	<body>
	<!---->
		<?php
			$data['active'] = 'group';
			echo View::make('template/nav',$data)->render();
		?>
	<!---->
		<div class="about">
			 <div class="container">
				 <h2>View Grup : <font color="black"><?php echo $targetGroup[0]->groupsname; ?></font></h2>
				 <center>
				 	<h3><?php echo $targetGroup[0]->groupsdesc; ?></h3>
				 	<h4><i>Ketua Grup : <?php echo $targetGroup[0]->groupslead; ?></i></h4>
					 <?php
					 	if (Session::get('msg')!=null) {
						   		echo "
						   		<br />	
						   		<div class='alert alert-success' role='alert'>
								<strong>Done! </strong>".Session::get('msg')."
						   		</div>";
						   	}
					 ?>
					
				 	<div class="about-grids">
				 		
					</div>

					<h4 class="b4">
						<?php
							if ($lead == 'lead') echo "<a href='".url()."/group/disbangroup/".$targetGroup[0]->groupsid."'<span class='label label-danger'>Bubarkan Grup</span></a>";
							else if ($lead == 'member') echo "<a href='".url()."/group/joingroup/".$targetGroup[0]->groupsid."'<span class='label label-success'>Gabung Grup</span></a>";
							else echo "<a href='".url()."/group/leavegroup/".$targetGroup[0]->groupsid."'<span class='label label-danger'>Keluar Grup</span></a>";
							
							
						?>
				  </h4>

				  <h4 class="b4">
				  		<?php echo "<a href='".url()."/jadwal/group/".$targetGroup[0]->groupsid."'<span class='label label-success'>List Jadwal</span></a>"; ?>
				  </h4>
</center>

					<div class="bs-docs-example">
				 	<div class="col-md-6 about-info">
						 <h3>List Anggota</h3>
					 </div>

						<table class="table table-hover">
							<thead>
								<tr>
								  <th>#</th>
								  <th>ID Anggota</th>
								  <th>Nama Anggota</th>
								  <th>Email Anggota</th>
								  <th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$ct=1;
									foreach($listMember as $data){
										echo "<tr>";
										echo "<td>".$ct."</td>";
										echo "<td>".$data->userid."</td>";
										echo "<td>".$data->name."</td>";
										echo "<td>".$data->email."</td>";

										if (Session::get('userid') == $targetGroup[0]->groupslead){
											if ($data->userid != Session::get('userid')){
												echo "
												<td>
													<a href='".url()."/group/kickfromgroup/".$targetGroup[0]->groupsid."/".$data->userid."'>Depak</a> | <a href='".url()."/group/giveleaderto/".$targetGroup[0]->groupsid."/".$data->userid."'>Jadikan Pengurus</a>
												</td>
												";
											} else {
												
											}
										}
										

										echo "</tr>";
										$ct++;	
									}
								?>
								
							</tbody>
						</table>
					</div>
			</div>


		</div>

		
	<!---->
		<?php
			echo View::make('template/foo')->render();
		?>
<!---->
	</body>
	
</html>