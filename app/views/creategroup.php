<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
				echo View::make('linker/linker_main_template')->render();
		?>
	</head>

	<body>
	<!---->
		<?php
			$data['active'] = 'group';
			echo View::make('template/nav',$data)->render();
		?>
	<!---->
		<div class="about">
			 <div class="container">
				 <h2>Buat Grup</h2>
				 	<div class="about-grids">
				 		<form method="post" action="<?php echo url();?>/group/creategroup/new">
				 			<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Nama Grup</span>
								<input type="text" class="form-control" placeholder="Nama Grup" aria-describedby="basic-addon1" name="groupsname">
							</div>

							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Deskripsi Grup</span>
								<input type="text" class="form-control" placeholder="Deskripsi Grup" aria-describedby="basic-addon1" name="groupsdesc">
							</div>
					
							<h4 class="b4">
									<span class="label label-default"><button type="submit">Buat Grup</button></span>		
							</h4>

				 </form>
					 
					</div>
			</div>
		</div>
	<!---->
		<?php
			echo View::make('template/foo')->render();
		?>
<!---->
	</body>
	
</html>