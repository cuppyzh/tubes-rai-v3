<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
				echo View::make('linker/linker_main_template')->render();
		?>
	</head>
	
	<body>
	<!---->
		<?php
			$data['active'] = 'group';
			echo View::make('template/nav',$data)->render();
		?>
		<!---->
		<div class="about">
			 <div class="container" style="height:92.5%;">
				 <h2>Grup</h2>
				  <div class="alerts">
					   <?php

					   	if (Session::get('msg')!=null) {
					   		echo "
					   		<div class='alert alert-success' role='alert'>
								<strong>Done! </strong>".Session::get('msg')."
					   		</div>";
					   	}

					   ?>
				    </div>
				 <div class="about-grids">
					 <div class="col-md-6 about-info">
						 <h5>Atur grup mu disini :)</h5>
						<h4 class="b4">
							<a href="<?php echo url(); ?>/group/creategroup"<span class="label label-success">Buat Grup</span></a>
						  </h4>

					 </div>
					


					 <div class="col-md-6 abt-pic">
						<!-- <img src="images/pic.jpg" class="img-responsive" alt=""/> -->
					 </div>
					 <div class="clearfix"></div>			 
				 </div>

				 <div class="bs-docs-example">
				 	<div class="col-md-6 about-info">
						 <h3>Grup</h3>
					 </div>
						<table class="table table-hover">
							<thead>
								<tr>
								  <th width='50'>#</th>
								  <th width='100'>ID Grup</th>
								  <th width='186'>Nama Goup</th>
								  <th width='186'>Deskripsi Goup</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$ct=1;
									foreach($group as $data){
										echo "<tr>";
										echo "<td>".$ct."</td>";
										echo "<td>".$data->groupsid."</td>";
										echo "<td><a href='".url()."/group/viewgroup/".$data->groupsid."'>".$data->groupsname."</a></td>";
										echo "<td>".$data->groupsdesc."</td>";
										echo "</tr>";
										$ct++;	
									}
								?>
								
							</tbody>
						</table>
					</div>

					<div class="bs-docs-example">
						 <div class="col-md-6 about-info">
						 <h3>Grup Lainnya</h3>
					 </div>
						<table class="table table-hover">
							<thead>
								<tr>
								  <th width='50'>#</th>
								  <th width='100'>ID Grup</th>
								  <th width='186'>Nama Goup</th>
								  <th width='186'>Deskripsi Goup</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$ct=1;
									foreach($allgroup as $datas){
										$flag=true;
										foreach ($group as $data) {
											if ($datas->groupsid == $data->groupsid) $flag=false;
										}
										if ($flag == true){
											echo "<tr>";
										echo "<td>".$ct."</td>";
										echo "<td>".$datas->groupsid."</td>";
										echo "<td><a href='".url()."/group/viewgroup/".$datas->groupsid."'>".$datas->groupsname."</a></td>";
										echo "<td>".$datas->groupsdesc."</td>";
										echo "</tr>";	
										$ct++;	
										if ($ct==11) break;
										}
										
									}
								?>
								
							</tbody>
						</table>
						<h4 class="b4">
						<a href="group/viewall"<span class="label label-default">Lihat Semua Grup</span></a>
					  </h4>

					</div>

				
				 
			 </div>
		</div>
		<!---->
		<?php
			echo View::make('template/foo')->render();
		?>
	<!---->
	</body>
</html>