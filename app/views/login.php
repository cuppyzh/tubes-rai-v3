<!DOCTYPE html>
<html>

	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
			echo View::make('linker/linker_main_template')->render();
		?>
	</head>

	<body>
		<?php
			$data['active'] = 'login';
			echo View::make('template/nav',$data)->render();
		?>
		<!-- LOGIN FORM -->
		<div class="container" style="height:92.5%;">
			<?php
				if (Session::get('errMessage')!=null) echo "<div class='errMsg'>".Session::get('errMessage')."</div><br />";
				else if (Session::get('sucMessage')) echo "<div class='sucMsg'>".Session::get('sucMessage')."</div><br />";
			?>
			<div class="distracted">
				 <div class="well">
			  		<h3 class="ghj" style="width"><center>Login</center></h3>
				  
						<form method="post" action="<?php echo url(); ?>/login/validate">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Username</span>
								<input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" name="username">
							</div>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Password</span>
								<input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" name="password">
							</div>
							<center>Tidak punya akun ? daftar <a href="<?Php echo url(); ?>/register">disini</a></center>
							<h3 class="b3">
								<button class="label label-success" type="submit">Login</button>
				  			</h2>


				   </div>
				  
		    </div>

			<!-- 
			<div class="login-form-1">
				<form id="login-form" class="text-left" method="post" action="<?php echo url(); ?>/login/validate">
					<div class="login-form-main-message"></div>
					<div class="main-login-form">
						<div class="login-group">
							<div class="form-group">
								<label for="lg_username" class="sr-only">Username</label>
								<input type="text" class="form-control" id="lg_username" name="username" placeholder="username">
							</div>
							<div class="form-group">
								<label for="lg_password" class="sr-only">Password</label>
								<input type="password" class="form-control" id="lg_password" name="password" placeholder="password">
							</div>
							<div class="form-group login-group-checkbox">
								<input type="checkbox" id="lg_remember" name="lg_remember">
								<label for="lg_remember">Ingat aku</label>
							</div>
						</div>
						<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i>O</button>
					</div>
					<div class="etc-login-form">
						
						<p>pengguna baru ? <a href="<?php echo url(); ?>/register">buat akun baru</a></p>
					</div>
				</form>
			</div>-->
			<!-- end:Main Form -->
		</div>
		<?php //echo "<div class='navbar-fixed-bottom'>"; 
			echo View::make('template/foo')->render();
		//"</div>"?>

	</body>

</html>