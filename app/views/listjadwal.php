
<!DOCTYPE html>
<html>
<head>
    <title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>

    <?php
        echo View::make('linker/linker_main_template')->render();
    ?>

</head>
<body>

    <?php
        $data['active'] = 'jadwal';
        echo View::make('template/nav',$data)->render();
    ?>
    <div class="about">
        <div class="container">
            <h2>List Jadwal</h2>

            <table class="table table-hover">
                            <thead>
                                <tr>
                                  <th width='50'>#</th>
                                  <th width='50'>ID Jadwal</th>
                                  <th width='100'>Deskripsi</th>
                                  <th width='186'>Prioritas</th>
                                  <th width='186'>Mulai</th>
                                  <th width='186'>Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $ct=1;
                                    foreach($listSchedules as $datas){
                                        //$flag=true;
                                        $prio = '';

                                        if ($datas['class'] == '0') $prio = 'Jawal Pribadi Prioritas Rendah ';
                                        else if ($datas['class'] == '1') $prio = 'Jadwal Pribadi Prioritas Tinggi';
                                        else if ($datas['class'] == '2') $prio = 'Jadwal Grup Prioritas Rendah';
                                        else if ($datas['class'] == '3') $prio = 'Jadwal Grup Prioritas Tinggi';
                                        else if ($datas['class'] == '4') $prio = 'Jadwal Lain';


                                        echo "<tr>";
                                        echo "<td>".$ct."</td>";
                                        echo "<td><a href='".url()."/jadwal/detail/".$datas['id']."'>".$datas['id']."</a></td>";
                                        echo "<td>".$datas['title']."</td>";
                                        echo "<td>".$prio."</td>";
                                        echo "<td>".$datas['start']."</td>";
                                        echo "<td>".$datas['end']."</td>";
                                        echo "</tr>";   

                                        $ct++;  
                                        }
                                ?>
                                
                            </tbody>
                        </table>

        </div>
    </div>

    <?php
        echo View::make('template/foo')->render();
    ?>

</body>
</html>
