<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
				echo View::make('linker/linker_main_template')->render();
			?>

	</head>
	<body>
	<!---->
		<?php
			$data['active'] = 'profile';
			echo View::make('template/nav',$data)->render();
		?>
		<!---->
		<div class="about">
			 <div class="container">
				 <h2>Profil</h2>
				 <div class="about-grids">
				 	 <div class="col-md-1 abt-pic">
						<img src="assets2/images/images.png" class="img-responsive" alt=""/>
					 </div>
					 <div class="col-md-6 about-info">
						 <h3>Selamat Datang, <font color="#33CD5F"><?php echo $profile[0]->userid; ?> </font></h3>
						 <h5>Disini tempat anda dapat mengatur akun anda.</h5>
						 <p></p>
					 </div>

					             

					 
					 <div class="clearfix"></div>			 
				 </div>
				 <br /> <br /> <br />
				  <table class="table table-bordered">
						
						<tbody>
							<tr>
								<td>Jumlah Grup</td>
								<td><span class="badge badge-success"><?php echo $numofgroups; ?></span></td>
							</tr>
							<tr>
								<td>Jumlah Jadwal Pribadi</td>
								<td><span class="badge badge-success"><?php echo $numofschedule; ?></span></td>
							</tr>
							<tr>
								<td>Jumlah Jadwal Keseluruhan</td>
								<td><span class="badge badge-success"><?php echo $totofschedule; ?></span></td>
							</tr>
							
						</tbody>
					  </table>       
				 <div class="charitys abt-chrits">
					  <div class="col-md-4 chrt_grid" style="visibility: visible; -webkit-animation-delay: 0.4s;">
						   <div class="chrty">
								<figure class="icon">
									 <span class="glyphicon-icon glyphicon-list-alt" aria-hidden="true"></span>
								</figure>
								<h3><a href="group">Grup</a> </h3>
								<p>Kelola group yang kamu punya disini!</p>
						  </div>
					  </div>
					  <div class="col-md-4 chrt_grid" style="visibility: visible; -webkit-animation-delay: 0.4s;">
						   <div class="chrty">
								<figure class="icon">
									<span class="glyphicon-icon glyphicon-stats" aria-hidden="true"></span>
								</figure>						
								<h3><a href="jadwal">Jadwal</a> </h3>
								<p>Tambah, hapus, atur jadwal dan cari jadwal disini tempatnya~</p>
						  </div>
					  </div>
					  <div class="col-md-4 chrt_grid" style="visibility: visible; -webkit-animation-delay: 0.4s;">
						   <div class="chrty">
								 <figure class="icon">
									<span class="glyphicon-icon glyphicon-th-large" aria-hidden="true"></span>
								</figure>						
								<h3>Mobile</h3>
								<p>Apliaksi ini tersedia juga versi mobilenya lho, cek disini *</p>
						  </div>
					  </div>
					  <div class="clearfix"></div>
				 </div>
				 
				 
			 </div>
		</div>
		<!---->
		<?php
			echo View::make('template/foo')->render();
		?>
	<!---->
	</body>
</html>