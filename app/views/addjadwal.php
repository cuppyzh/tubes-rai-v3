<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Sinkin Ship.dev | Pencari Jadwal Kosong</title>
		<?php
				echo View::make('linker/linker_main_template')->render();
				$data['group'] = $group;
				echo View::make('linker/linker_date_picker', $data)->render();
		?>
		
	</head>

	<body>

		<?php
			$data['active'] = 'group';
			echo View::make('template/nav',$data)->render();
		?>

		<div class="about">
			 <div class="container">
				 <h2>Tambah Jadwal</h2>
				 <div class="alerts">
                	<?php

                    if (Session::get('errMsg')!=null) {
                        echo "
                            <div class='alert alert-danger' role='alert'>
                                <strong>Done! </strong>".Session::get('errMsg')."
                            </div>";
                   	} else if (Session::get('sucMsg')!=null) {
                        echo "
                            <div class='alert alert-success' role='alert'>
                                <strong>Hold! </strong>".Session::get('sucMsg')."
                            </div>";
                    }

                    ?>
            	</div>
            	<h3>Masukan Data Jadwal : </h3>
				 	<div class="about-grids">
				 		<form method="post" action="<?php echo url();?>/jadwal/add/make">
				 			<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">Deskripsi Jadwal</span>
								<input type="text" class="form-control" placeholder="Deskripsi" aria-describedby="basic-addon1" name="desc">
							</div>

							<div class="row">

								<div class="col-lg-6 in-gp-tl">
									<div class="input-group">
										<span class="input-group-addon">
											<input type="radio" aria-label="..." name="jenis" value="personal">
										</span>
										<input type="text" class="form-control" aria-label="..." placeholder="Personal" readonly>
									</div>
								</div>

								<div class="col-lg-6 in-gp-tb">
									<div class="input-group">
										<span class="input-group-addon">
											<input class="group-check-js" type="radio" aria-label="..." name="jenis" value="group">
										</span>
										<input type="text" class="form-control" aria-label="..." placeholder="Grup" readonly>
									</div>
								</div>
								
							</div>

							<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Grup : </span>
									<select name="groupid" class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown" aria-haspopup="true" aria-describedby="basic-addon1">
									<?php
	                                    foreach ($group as $row) {
	                                        echo "<option value=".$row->groupsid.">".$row->groupsname."</option>";
	                                    }
                                	?>
                                	</select>
							</div>

							<!--<div id="surpriseeee">
							</div>-->

							<div class="row">

								<div class="col-lg-6 in-gp-tl">
									<div class="input-group">
										<span class="input-group-addon">
											<input type="radio" aria-label="..." name="priority" value="rendah">
										</span>
										<input type="text" class="form-control" aria-label="..." placeholder="Rendah" readonly>
									</div>
								</div>

								<div class="col-lg-6 in-gp-tb">
									<div class="input-group">
										<span class="input-group-addon">
											<input type="radio" aria-label="..." name="priority" value="tinggi">
										</span>
										<input type="text" class="form-control" aria-label="..." placeholder="Tinggi" readonly>
									</div>
								</div>
								
							</div>

							<div class="input-group">
									<span class="tglmulai"><img src="http://localhost:8080/tubes-rai-v3/public/assets_datepicker/images/calendar.png" /></span>Tanggal Mulai
									<input name="tglmulai" id="tglmulaiinput" type="text" class="form-control" value="Klik Kalender" readonly>

							</div>

							<div class="input-group">
									<span class="tglakhir"><img src="http://localhost:8080/tubes-rai-v3/public/assets_datepicker/images/calendar.png" /></span>Tanggal Mulai
									<input name="tglakhir" id="tglakhirinput" type="text" class="form-control" value="Klik Kalender" aria-describedby="basic-addon1" readonly>
							</div>
					
							<h4 class="b4">
									<span class="label label-success"><button type="submit">Tambah Jadwal</button></span>		
							</h4>

				 </form>
					 
					</div>
			</div>
		</div>
	<!---->
		<?php
			echo View::make('template/foo')->render();
		?>
<!----><script src="<?php echo url(); ?>/assets_datepicker/src/datepickr.min.js"></script>
		<script>
            // datepickr on an icon, using altInput to store the value
            // altInput must be a direct reference to an input element (for now)
            datepickr('.tglmulai', { altInput: document.getElementById('tglmulaiinput') });
            datepickr('.tglakhir', { altInput: document.getElementById('tglakhirinput') });
            // If the input contains a value, datepickr will attempt to run Date.parse on it
            datepickr('[title="parseMe"]');

            // Overwrite the global datepickr prototype
            // Won't affect previously created datepickrs, but will affect any new ones
            datepickr.prototype.l10n.months.shorthand = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            datepickr.prototype.l10n.months.longhand = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            datepickr.prototype.l10n.weekdays.shorthand = ['dim', 'lun', 'mar', 'mer', 'jeu', 'ven', 'sam'];
            datepickr.prototype.l10n.weekdays.longhand = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
            datepickr('#someFrench.sil-vous-plait', { dateFormat: '\\Da\\y picke\\d: Y/m/d' });
    	</script>
	</body>
	
</html>